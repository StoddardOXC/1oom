# This is a PBXIN file. It enables all the optional MOO1 bug fixes.
# Use "1oom_pbxmake pbxin_fixbugs.txt fixbugs.pbx" to make the PBX file.
# Then "1oom_classic_sdl1 -file fixbugs.pbx" to use it.

0,"Fix MOO1 bugs"
1,"Enables all optional MOO1 bug fixes.\nAlso gives Guardian Advanced Damage Control on impossible."

# Give Guardian Advance Damage Control on impossible.
# MOO1 has Automated Repair (but shows ADC) on hard and nothing on impossible.
# The MOO1 code is along the lines of:
#   if (difficulty == hard) { special2 = ADC; } else if (difficulty == hard) { special2 = AR; }
#   if (difficulty == hard) { repair = 15; } else if (difficulty == hard) { repair = 30; }
# This is clearly a bug.
4,guardian_special2,3,16,26
4,guardian_repair,4,30

# See doc/list_pbxnum.txt for description of the rest of the fixes.
4,bt_wait_no_reload,0,1
4,bt_precap_tohit,0,1
4,bt_no_tohit_acc,0,1
4,news_orion,0,1
4,orbital_weap_any,0,1
4,orbital_weap_4,0,1
4,combat_trans_fix,0,1
4,stargate_redir_fix,0,1
4,trans_redir_fix,0,1
